/**
 *  Change React Proptypes in Proptypes
 *  Make sure the code style match yours in the constants
 *
 *  Execute it on each file of a folder :
 */
//  find ./your-path/**/*.{js,jsx} -maxdepth 9 -type f -exec node PropTypesConverter.js {} \;

var fs = require('fs')

const filePath = process.argv[2];

const REACT_PROPTYPES = "React.PropTypes";

const REQUIRE_IMPORT = `const React = require("react");`;
const SIMPLE_IMPORT = `import React from "react";`;
const DESCTRUCT_IMPORT = `import React, {PropTypes} from "react";`;
const DESCTRUCT_IMPORT_2 = `import {PropTypes}, React from "react";`;

const IMPORT_PROPTYPES = `import PropTypes from "prop-types";`;
const REQUIRE_PROPTYPES = `const PropTypes = require("prop-types");`;

const NEW_IMPORT = `import React from "react";
${IMPORT_PROPTYPES}`;

const NEW_REQUIRE = `const React = require("react");
${REQUIRE_PROPTYPES}`;

function dataContains(data, arrayOfSubString) {
  return arrayOfSubString.some(function(v) { return data.indexOf(v) >= 0; })
}

fs.readFile(filePath, 'utf8', function (err, data) {
  if (err) {
    return console.log(err);
  }

  // if there are React.PropTypes in file
  if (data.indexOf(REACT_PROPTYPES) !== -1) {
    let new_data = data;

    // if proptypes isn't already imported
    if (!dataContains(data, [IMPORT_PROPTYPES, REQUIRE_PROPTYPES])) {
      new_data = new_data
        .replace(SIMPLE_IMPORT, NEW_IMPORT)
        .replace(DESCTRUCT_IMPORT, NEW_IMPORT)
        .replace(REQUIRE_IMPORT, NEW_REQUIRE);
    }

    new_data = new_data.replace(REACT_PROPTYPES, "PropTypes");

    fs.writeFile(filePath, new_data, 'utf8', function (err) {
      if (err) return console.log(err);
      console.log(`${filePath} updated`);
    });
  }
});
