/**
 *  Decompress an lz string pass in parameter
 *  Make sure you have lz-string install in your node_modules
 *  
 *  node decompressFromEncodedURI.js "[the compressed string]"
 */

const lz = require("lz-string");
console.log("decompress : " + process.argv[2]);
console.log("---------");
console.log(lz.decompressFromEncodedURIComponent(process.argv[2]));
console.log("---------");
